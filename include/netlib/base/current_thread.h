#ifndef _CURRENT_THREAD_H_
#define _CURRENT_THREAD_H_

#include <string>

namespace netlib::current_thread {
extern __thread int t_tid_cache;
extern __thread char t_tid_str[32];
extern __thread int t_tid_str_len;
extern __thread const char* t_name;

void CacheTid();

inline int Tid() {
	if (__builtin_expect(t_tid_cache == 0, 0)) {
		CacheTid();
	}
	return t_tid_cache;
}

inline const char* TidStr() { return t_tid_str; }

inline int TidStrLength() { return t_tid_str_len; }

std::string StackTrace(bool demangle);

} // namespace netlib::current_thread

#endif // _CURRENT_THREAD_H_