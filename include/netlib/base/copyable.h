#ifndef _COPYABLE_H_
#define _COPYABLE_H_

class Copyable {
protected:
	Copyable() = default;
	~Copyable() = default;
};

#endif // _COPYABLE_H_