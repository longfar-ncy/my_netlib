#ifndef EXCEPTION_H_
#define EXCEPTION_H_

#include <exception>
#include <string>

namespace netlib {

class Exception : std::exception {
public:
	explicit Exception(const std::string& what);
	explicit Exception(std::string&& what);
	~Exception() noexcept override = default;

	// default copy-ctor and operator= are okay.

	const char* what() const noexcept override { return message_.c_str(); }

	const char* StackTrace() const noexcept { return stack_.c_str(); }

private:
	std::string message_;
	std::string stack_;
};

} // namespace netlib

#endif // EXCEPTION_H_