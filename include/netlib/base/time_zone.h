// Use of this source code is governed by a BSD-style license
// that can be found in the License file.
//
// Author: Shuo Chen (chenshuo at chenshuo dot com)

#ifndef _TIMEZONE_H_
#define _TIMEZONE_H_

#include <ctime>
#include <memory>

#include "netlib/base/copyable.h"

namespace netlib {

// TimeZone for 1970~2030
class TimeZone : Copyable {
public:
	explicit TimeZone(const char* zonefile);
	TimeZone(int eastOfUtc, const char* tzname); // a fixed timezone
	TimeZone() = default;                        // an invalid timezone

	// default copy ctor/assignment/dtor are Okay.

	bool Valid() const {
		// 'explicit operator bool() const' in C++11
		return static_cast<bool>(data_);
	}

	struct tm ToLocalTime(time_t secondsSinceEpoch) const;
	time_t FromLocalTime(const struct tm& /*localTm*/) const;

	// gmtime(3)
	static struct tm ToUtcTime(time_t secondsSinceEpoch, bool yday = false);
	// timegm(3)
	static time_t FromUtcTime(const struct tm& /*utc*/);
	// year in [1900..2500], month in [1..12], day in [1..31]
	static time_t FromUtcTime(int year, int month, int day, int hour, int minute, int seconds);

	struct Data;

private:
	std::shared_ptr<Data> data_;
};

} // namespace netlib

#endif // MUDUO_BASE_TIMEZONE_H
