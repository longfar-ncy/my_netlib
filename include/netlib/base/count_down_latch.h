#ifndef COUNT_DOWN_LATCH_H_
#define COUNT_DOWN_LATCH_H_

#include "netlib/base/condition.h"
#include "netlib/base/mutex.h"

namespace netlib {

class CountDownLatch {
public:
	explicit CountDownLatch(int count) : count_(count) {}

	void CountDown();
	void Wait();

	int GetCount() const;

private:
	mutable MutexLock mutex_;
	Condition cond_{mutex_};
	int count_;
};

} // namespace netlib

#endif // COUNT_DOWN_LATCH_H_