#ifndef CONDITION_H_
#define CONDITION_H_

#include "netlib/base/mutex.h"
#include "netlib/base/noncopyable.h"

namespace netlib {

class Condition : NonCopyable {
public:
	explicit Condition(MutexLock& mutex) : mutex_(mutex) {
		MCHECK(pthread_cond_init(&cond_, nullptr));
	}
	~Condition() { MCHECK(pthread_cond_destroy(&cond_)); }

	void Wait() {
		MutexLock::UnassignGuard ug(mutex_);
		MCHECK(pthread_cond_wait(&cond_, mutex_.GetPthreadMutex()));
	}
	void Notify() { MCHECK(pthread_cond_signal(&cond_)); }
	void NotifyAll() { MCHECK(pthread_cond_broadcast(&cond_)); }
	bool WaitForSeconds(double seconds);

private:
	MutexLock& mutex_;
	pthread_cond_t cond_;
};

} // namespace netlib

#endif // CONDITION_H_