#ifndef SOCKET_OPS_H_
#define SOCKET_OPS_H_

#include <arpa/inet.h>

namespace netlib::net {

class InetAddress;

namespace sockets {

void BindOrDie(int fd, const sockaddr* addr);
void ListenOrDie(int fd);
const sockaddr* SockaddrCast(const sockaddr_in* addr);
const sockaddr* SockaddrCast(const sockaddr_in6* addr);
int Accept(int sockfd, struct sockaddr_in6* addr);
int Connect(int sockfd, const struct sockaddr* addr);
void FromIpPort(const char* ip, uint16_t port, sockaddr_in* addr);
void FromIpPort(const char* ip, uint16_t port, sockaddr_in6* addr);
InetAddress GetLocalAddress(int fd);
void ToIpPort(char* buf, size_t size, const struct sockaddr* addr);
void ToIp(char* buf, size_t size, const struct sockaddr* addr);
const sockaddr_in* SockaddrInCast(const struct sockaddr* addr);
const sockaddr_in6* SockaddrIn6Cast(const struct sockaddr* addr);
int GetSocketError(int sockfd);
void ShutdownWrite(int sockfd);
void Close(int sockfd);
bool IsSelfConnect(int /*sockfd*/);
sockaddr_in6 GetLocalAddr(int sockfd);
sockaddr_in6 GetPeerAddr(int sockfd);

inline uint16_t NetworkToHost16(uint16_t net16) { return be16toh(net16); }

} // namespace sockets
} // namespace netlib::net

#endif // SOCKET_OPS_H_