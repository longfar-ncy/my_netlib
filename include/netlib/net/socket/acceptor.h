#ifndef ACCEPTOR_H_
#define ACCEPTOR_H_

#include "netlib/base/noncopyable.h"
#include "netlib/net/event/channel.h"
#include "netlib/net/socket/inet_address.h"
#include "netlib/net/socket/socket.h"

namespace netlib::net {

class EventLoop;

class Acceptor : NonCopyable {
public:
	using NewConnectionCallback = std::function<void(int, const InetAddress&)>;

	Acceptor(EventLoop* loop, const InetAddress& addr);
	~Acceptor();

	bool Listening() const { return is_listening_; }
	void SetNewConnectionCallback(const NewConnectionCallback& cb) { callback_ = cb; }

	void Listen();

private:
	void HandleRead();
	// int accept();

	bool is_listening_{false};
	EventLoop* loop_;
	Socket accept_sock_;
	// int sockfd_;
	Channel accept_channel_;
	NewConnectionCallback callback_;
};

} // namespace netlib::net

#endif // ACCEPTOR_H_