#ifndef SIGPIPE_IGNORE_H_
#define SIGPIPE_IGNORE_H_

#include <sys/signal.h>

namespace netlib::net::detail {

class SigPipeIgnore {
public:
	SigPipeIgnore() { signal(SIGPIPE, SIG_IGN); }
};

SigPipeIgnore g_sigpipe_ignore;

} // namespace netlib::net::detail

#endif // SIGPIPE_IGNORE_H_