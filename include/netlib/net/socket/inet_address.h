#ifndef INET_ADDRESS_H_
#define INET_ADDRESS_H_

#include <arpa/inet.h>
#include <sys/socket.h>

#include <string>

#include "netlib/base/string_piece.h"

namespace netlib::net {

class InetAddress {
public:
	InetAddress() = default;
	explicit InetAddress(uint32_t port) : InetAddress(StringArg("127.0.0.1"), port) {}
	explicit InetAddress(const sockaddr_in6& addr) : addr6_(addr) {}
	InetAddress(StringArg ip, uint16_t port, bool ipv6 = false);

	sa_family_t Family() const { return addr_.sin_family; }
	const sockaddr* GetSockaddr() const;
	void SetAddr6(const sockaddr_in6& addr) { addr6_ = addr; }
	std::string ToIpPort() const;

private:
	union {
		sockaddr_in addr_;
		sockaddr_in6 addr6_;
	};
};

} // namespace netlib::net

#endif // INET_ADDRESS_H_
