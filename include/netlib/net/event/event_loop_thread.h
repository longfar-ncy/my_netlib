#ifndef EVENT_LOOP_THREAD_H_
#define EVENT_LOOP_THREAD_H_

// #include

#include "netlib/base/condition.h"
#include "netlib/base/mutex.h"
#include "netlib/base/noncopyable.h"
#include "netlib/base/thread.h"
#include "netlib/net/call_backs.h"

namespace netlib::net {

class EventLoop;

class EventLoopThread : NonCopyable {
public:
	explicit EventLoopThread(bool is_epoll = false,
	                         const ThreadInitCallBack& cb = ThreadInitCallBack(),
	                         const std::string& name = "");
	~EventLoopThread();

	EventLoop* StartLoop();

private:
	void ThreadFunc();

	MutexLock mutex_;
	Condition cond_{mutex_};
	EventLoop* loop_{nullptr};
	Thread thread_;
	ThreadInitCallBack init_cb_;
	bool is_epoll_;
};

} // namespace netlib::net

#endif // EVENT_LOOP_THREAD_H_