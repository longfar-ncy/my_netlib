#ifndef EVENT_LOOP_THREADPOOL_H_
#define EVENT_LOOP_THREADPOOL_H_

#include <cassert>
#include <string>
#include <vector>

#include "netlib/base/noncopyable.h"
#include "netlib/net/call_backs.h"
// #include "netlib/net/event/event_loop_thread.h"

namespace netlib::net {

class EventLoop;
class EventLoopThread;

class EventLoopThreadPool : NonCopyable {
public:
	explicit EventLoopThreadPool(EventLoop* loop,
	                             bool is_epoll = false,
	                             int n = 0,
	                             std::string name = "threadpool");
	~EventLoopThreadPool();

	void SetThreadsNum(int n) {
		assert(!is_started_);
		threads_num_ = n;
	}
	// void setThreadInitCallback(const Functor& cb) {
	// 	assert(!is_started_);
	// 	thread_init_cb_ = cb;
	// }
	std::string Name() const { return name_; }
	bool Started() const { return is_started_; }
	EventLoop* GetNextLoop();

	void Start(const ThreadInitCallBack& cb = ThreadInitCallBack());

private:
	bool is_started_{false};
	bool is_epoll_;
	int threads_num_;
	std::string name_;
	EventLoop* base_loop_;
	std::vector<EventLoopThread*> threads_;
	std::vector<EventLoop*> loops_;
	int next_loop_{0};
	// Functor thread_init_cb_;
};

} // namespace netlib::net

#endif // EVENT_LOOP_THREADPOOL_H_