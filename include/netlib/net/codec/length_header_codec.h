#ifndef _LENGTH_HEADER_CODEC_H_
#define _LENGTH_HEADER_CODEC_H_

#include <string>

#include "netlib/base/noncopyable.h"
#include "netlib/net/socket/tcp_connection.h"

using namespace netlib; // NOLINT

class LengthHeaderCodec : NonCopyable {
public:
	using StringMessageCallback =
	    std::function<void(const net::TcpConnectionPtr&, const std::string&, Timestamp)>;
	explicit LengthHeaderCodec(StringMessageCallback&& cb) : msg_callback_(cb) {}

	void Send(const net::TcpConnectionPtr& conn, const std::string& msg);
	void OnMessage(const net::TcpConnectionPtr& conn, net::Buffer* buf, Timestamp time);

private:
	StringMessageCallback msg_callback_;
	static const unsigned kHeaderLen = sizeof(int32_t);
};

#endif // _LENGTH_HEADER_CODEC_H_