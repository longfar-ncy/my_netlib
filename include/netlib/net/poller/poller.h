#ifndef POLLER_H_
#define POLLER_H_

#include <map>
#include <vector>

#include "netlib/base/noncopyable.h"
#include "netlib/base/time_stamp.h"

namespace netlib::net {

class EventLoop;
class Channel;

class Poller : NonCopyable {
public:
	using ChannelVector = std::vector<Channel*>;

	explicit Poller(EventLoop* loop) : owner_loop_(loop) {}
	virtual ~Poller() = default;

	virtual Timestamp Poll(int timeout, ChannelVector* activate_channels) = 0;
	void UpdateChannel(Channel* channel);
	void RemoveChannel(Channel* channel);

	static Poller* GetPoller(EventLoop* loop, bool is_epoll = false);

protected:
	virtual void FillActivateChannels(int n_events, ChannelVector* activet_channels) const = 0;
	virtual void AddChannel(Channel* channel) = 0;
	virtual void ResetChannel(Channel* channel) = 0;
	virtual void RemovePollfd(Channel* channel) = 0;

	// private:
	using ChannelMap = std::map<int, Channel*>;

	EventLoop* owner_loop_;
	ChannelMap channels_;
};

} // namespace netlib::net

#endif // POLLER_H_