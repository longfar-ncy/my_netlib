#ifndef EPOLLPOLLER_H_
#define EPOLLPOLLER_H_

#include "poller.h"

struct epoll_event;

namespace netlib::net {

// struct epoll_event;

class EPollPoller : public Poller {
public:
	explicit EPollPoller(EventLoop* loop);
	~EPollPoller() override;

	Timestamp Poll(int timeout, ChannelVector* activate_channels) override;

private:
	void FillActivateChannels(int n_events, ChannelVector* activate_channels) const override;
	void AddChannel(Channel* channel) override;
	void ResetChannel(Channel* channel) override;
	void RemovePollfd(Channel* channel) override;

	int epollfd_;
	std::vector<::epoll_event> events_;

	static const int kInitEventListSize = 16;
};

} // namespace netlib::net

#endif // EPOLLPOLLER_H_