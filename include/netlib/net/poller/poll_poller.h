#ifndef POLLPOLLER_H_
#define POLLPOLLER_H_

#include <sys/poll.h>

#include "poller.h"

// struct pollfd;

namespace netlib::net {

class EventLoop;

class PollPoller : public Poller {
public:
	explicit PollPoller(EventLoop* loop) : Poller::Poller(loop) {}

	Timestamp Poll(int timeout, ChannelVector* activate_channels) override;

private:
	void FillActivateChannels(int n_events, ChannelVector* activate_channels) const override;
	void AddChannel(Channel* channel) override;
	void ResetChannel(Channel* channel) override;
	void RemovePollfd(Channel* channel) override;

	std::vector<::pollfd> pollfds_;
};

} // namespace netlib::net

#endif // POLLPOLLER_H_