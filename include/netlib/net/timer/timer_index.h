#ifndef TIMER_IDX_H_
#define TIMER_IDX_H_

#include "netlib/base/copyable.h"
#include "netlib/base/time_stamp.h"

namespace netlib::net {

class Timer;

class TimerIndex : Copyable {
public:
	// TimerIndex() = default;

	explicit TimerIndex(Timer* timer);
	TimerIndex(Timestamp time, int64_t seq) : time_(time), sequence_(seq) {}

	// default copy-ctor, dtor and assignment are okay

	friend class TimerQueue;

private:
	// Timer* timer_;
	Timestamp time_;
	int64_t sequence_;
};

// bool operator<(const)

} // namespace netlib::net

#endif // TIMER_IDX_H_