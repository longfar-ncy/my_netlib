#include "netlib/net/poller/poller.h"

#include "netlib/net/event/channel.h"
#include "netlib/net/event/event_loop.h"
#include "netlib/net/poller/epoll_poller.h"
#include "netlib/net/poller/poll_poller.h"

void netlib::net::Poller::UpdateChannel(Channel* channel) {
	owner_loop_->AssertInLoopThread();
	if (-1 == channel->Index()) {
		// if (!channel->isNoneEvent())
		AddChannel(channel);
	} else {
		ResetChannel(channel);
	}
}

void netlib::net::Poller::RemoveChannel(Channel* channel) {
	owner_loop_->AssertInLoopThread();
	assert(channels_.find(channel->Fd()) != channels_.end());
	assert(channels_.at(channel->Fd()) == channel);
	assert(channel->IsNoneEvent());
	channels_.erase(channel->Fd());
	RemovePollfd(channel);
}

netlib::net::Poller* netlib::net::Poller::GetPoller(EventLoop* loop, bool is_epoll) {
	if (is_epoll) {
		return new EPollPoller(loop);
	}
	return new PollPoller(loop);
}
