#include "netlib/net/timer/timer_index.h"

#include "netlib/net/timer/timer.h"

netlib::net::TimerIndex::TimerIndex(Timer* timer) :
    time_(timer->Expiration()), sequence_(timer->Sequence()) {}