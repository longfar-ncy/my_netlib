#include "netlib/net/socket/inet_address.h"

#include "netlib/net/socket/socket_ops.h"

using std::string;

netlib::net::InetAddress::InetAddress(StringArg ip, uint16_t port, bool ipv6) {
	memset(&addr6_, '\0', sizeof addr6_);
	if (ipv6 || strchr(ip.CStr(), ':')) {
		sockets::FromIpPort(ip.CStr(), port, &addr6_);
	} else {
		sockets::FromIpPort(ip.CStr(), port, &addr_);
	}
}

const sockaddr* netlib::net::InetAddress::GetSockaddr() const {
	return sockets::SockaddrCast(&addr_);
}

string netlib::net::InetAddress::ToIpPort() const {
	char buf[64] = "";
	sockets::ToIpPort(buf, sizeof buf, GetSockaddr());
	return buf;
}