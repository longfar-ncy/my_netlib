#include "netlib/base/log_stream.h"

#include <cstring>

netlib::LogStream::OutputFunc netlib::LogStream::output_func = netlib::LogStream::DefaultOutput;

netlib::LogStream::FlushFunc netlib::LogStream::flush_func = netlib::LogStream::DefaultFlush;

netlib::LogStream& netlib::LogStream::operator<<(bool val) {
	data_.Append(val ? "1" : "0", 1);
	return *this;
}
netlib::LogStream& netlib::LogStream::operator<<(char val) {
	data_.Append(&val, 1);
	return *this;
}

netlib::LogStream& netlib::LogStream::operator<<(int16_t val) {
	data_.AppendInteger(val);
	return *this;
}

netlib::LogStream& netlib::LogStream::operator<<(int val) {
	data_.AppendInteger(val);
	return *this;
}

netlib::LogStream& netlib::LogStream::operator<<(unsigned val) {
	data_.AppendInteger(val);
	return *this;
}

netlib::LogStream& netlib::LogStream::operator<<(int64_t val) {
	data_.AppendInteger(val);
	return *this;
}

netlib::LogStream& netlib::LogStream::operator<<(uint64_t val) {
	data_.AppendInteger(val);
	return *this;
}

netlib::LogStream& netlib::LogStream::operator<<(double val) {
	data_.AppendDouble(val);
	return *this;
}

netlib::LogStream& netlib::LogStream::operator<<(float val) {
	data_.AppendDouble(static_cast<double>(val));
	return *this;
}

netlib::LogStream& netlib::LogStream::operator<<(const char* str) {
	if (str) {
		data_.Append(str, static_cast<int>(strlen(str)));
	} else {
		data_.Append("(null)", 6);
	}
	return *this;
}

netlib::LogStream& netlib::LogStream::operator<<(const std::string& str) {
	data_.Append(str.c_str(), str.size());
	return *this;
}

netlib::LogStream& netlib::LogStream::operator<<(std::string&& str) {
	data_.Append(str.c_str(), str.size());
	return *this;
}

netlib::LogStream& netlib::LogStream::operator<<(KnowSize&& obj) {
	data_.Append(obj.data_, obj.len_);
	return *this;
}

netlib::LogStream& netlib::LogStream::operator<<(const BaseName& obj) {
	data_.Append(obj.data_, obj.size_);
	return *this;
}