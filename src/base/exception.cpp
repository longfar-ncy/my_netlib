#include "netlib/base/exception.h"

#include "netlib/base/current_thread.h"

netlib::Exception::Exception(const std::string& what) :
    message_(what), stack_(current_thread::StackTrace(false)) {}

netlib::Exception::Exception(std::string&& what) :
    message_(what), stack_(current_thread::StackTrace(false)) {}