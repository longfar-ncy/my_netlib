#include "netlib/base/count_down_latch.h"

void netlib::CountDownLatch::CountDown() {
	MutexLockGuard lock(mutex_);
	if (0 == --count_) {
		cond_.NotifyAll();
	}
}

void netlib::CountDownLatch::Wait() {
	MutexLockGuard lock(mutex_);
	while (count_ > 0) {
		cond_.Wait();
	}
}

int netlib::CountDownLatch::GetCount() const {
	MutexLockGuard lock(mutex_);
	return count_;
}
