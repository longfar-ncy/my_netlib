#include "netlib/base/condition.h"

bool netlib::Condition::WaitForSeconds(double seconds) {
	static const int kNSecPerSec = 1000000000;
	struct timespec time;
	clock_gettime(CLOCK_REALTIME, &time);
	int64_t nseconds = static_cast<int64_t>(seconds) * kNSecPerSec;
	time.tv_sec += nseconds / kNSecPerSec;
	time.tv_nsec += nseconds % kNSecPerSec;
	MutexLock::UnassignGuard u_guard(mutex_);
	return ETIMEDOUT == pthread_cond_timedwait(&cond_, mutex_.GetPthreadMutex(), &time);
}