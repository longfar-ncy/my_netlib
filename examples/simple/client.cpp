#include <iostream>

#include "netlib/base/logger.h"
#include "netlib/net/event/event_loop_thread.h"
#include "netlib/net/socket/inet_address.h"
#include "netlib/net/tcp_client.h"

int main() {
	using namespace netlib;
	net::InetAddress addr("127.0.0.1", 28709);            // 目标地址
	net::EventLoopThread loop_thread;                     // 事件循环线程
	net::TcpClient client(loop_thread.StartLoop(), addr); // 构造客户端
	client.SetMessageCallback(                            // 设置处理收到消息的回调函数
	    [](const net::TcpConnectionPtr& conn, net::Buffer* buf, Timestamp ts) {
		    std::string msg(buf->RetrieveAllAsString());
		    std::cout << "Receive: " << msg << std::endl; // NOLINT
	    });
	client.Connect();            // 客户端连接服务端，成功or失败后返回
	if (!client.IsConnected()) { // 若连接不成功，则退出程序
		LOG_FATAL << "Failed to connect to server";
	}

	// 读取输入的每一行字符串并通过客户端发送给服务端，直到 exit 或 quit
	std::string line;
	while (std::getline(std::cin, line)) {
		if (line == "exit" || line == "quit") {
			break;
		}
		client.Send(line);
	}

	client.Disconnect(); // 断开连接
}