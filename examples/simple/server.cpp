#include "netlib/net/event/event_loop.h"
#include "netlib/net/socket/buffer.h"
#include "netlib/net/socket/tcp_connection.h"
#include "netlib/net/tcp_server.h"

int main() {
	using namespace netlib;
	net::InetAddress addr("127.0.0.1", 28709); // 监听端口
	net::EventLoop loop;                       // 事件监听主循环
	net::TcpServer server(&loop, addr);        // 构造 server
	server.SetMessageCallback(                 // 为 server 设置处理消息的回调函数
	    [](const net::TcpConnectionPtr& conn, net::Buffer* buf, Timestamp ts) {
		    std::string msg(buf->RetrieveAllAsString());
		    LOG_INFO << conn->Name() << " echo " << msg.size() << "bytes, "
		             << "data received at " << ts.ToString();
		    conn->Send(msg);
	    });
	server.Start(); // 服务器开始监听
	loop.Loop();    // 主事件循环开始
}