#ifndef _CHAT_SERVER_H_
#define _CHAT_SERVER_H_

#include <set>

#include "netlib/net/call_backs.h"
#include "netlib/net/codec/length_header_codec.h"
#include "netlib/net/tcp_server.h"

using namespace netlib; // NOLINT

class ChatServer {
public:
	ChatServer(net::EventLoop* loop, net::InetAddress addr);
	void Start() { server_.Start(); }

private:
	void OnConnection(const net::TcpConnectionPtr& conn);
	void OnStringMessage(const net::TcpConnectionPtr& conn, const std::string& msg, Timestamp time);

private:
	using ConnectionSet = std::set<net::TcpConnectionPtr>;

	net::EventLoop* loop_;
	net::TcpServer server_;
	LengthHeaderCodec codec_;
	ConnectionSet connections_;
};

#endif // _CHAT_SERVER_H_