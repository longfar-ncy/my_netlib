#include "chat_server.h"
#include "netlib/base/logger.h"
#include "netlib/net/event/event_loop.h"

int main() {
	LOG_INFO << "pid = " << getpid();
	net::EventLoop loop;
	net::InetAddress listen_addr(6666);
	ChatServer server(&loop, listen_addr);
	server.Start();
	loop.Loop();
	return 0;
}