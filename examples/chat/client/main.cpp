#include <iostream>

#include "chat_client.h"
#include "netlib/base/logger.h"
#include "netlib/net/event/event_loop_thread.h"

// void testGetline() {
// 	string line;
// 	while (std::getline(std::cin, line)) {
// 		printf("%s\n", line.c_str());
// 	}
// }

int main() {
	LOG_INFO << "pid = " << getpid();
	net::InetAddress serv_addr(6666);
	net::EventLoopThread loop_thread;
	ChatClient client(loop_thread.StartLoop(), serv_addr);
	client.Connect();
	std::string line;
	while (std::getline(std::cin, line)) {
		client.Write(line);
	}
	client.Disconnect();
	// testGetline();
}