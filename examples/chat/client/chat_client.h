#ifndef _CHAT_CLIENT_H_
#define _CHAT_CLIENT_H_

#include <string>

#include "netlib/base/mutex.h"
#include "netlib/base/noncopyable.h"
#include "netlib/net/codec/length_header_codec.h"
#include "netlib/net/tcp_client.h"

using namespace netlib; // NOLINT

class ChatClient : NonCopyable {
public:
	ChatClient(net::EventLoop* loop, const net::InetAddress& serv_addr);

	void Connect() { client_.Connect(); }
	void Disconnect() { client_.Disconnect(); }

	void Write(const std::string& msg);

private:
	void OnConnection(const net::TcpConnectionPtr& conn);
	void OnStringMessage(const net::TcpConnectionPtr& conn, const std::string& msg, Timestamp time);

private:
	net::EventLoop* loop_;
	net::TcpClient client_;
	mutable MutexLock mutex_;
	net::TcpConnectionPtr connect_;
	LengthHeaderCodec codec_;
};

#endif // _CHAT_CLIENT_H_