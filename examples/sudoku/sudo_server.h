#ifndef _SUDO_SERVER_H_
#define _SUDO_SERVER_H_

#include "netlib/net/tcp_server.h"

using namespace netlib; // NOLINT

class SudoServer {
public:
	SudoServer(net::EventLoop* loop, const net::InetAddress& addr, int n_threads);
	void Start();

private:
	void OnConnection(const net::TcpConnectionPtr& conn);
	void OnMessage(const net::TcpConnectionPtr& conn, net::Buffer* buf, Timestamp time);

	static std::string SolveSudo(const std::string& puzzle);
	static bool Backtracking(std::string& result, int idx);
	static bool IsOk(const std::string& result, int idx, int val);

	static void
	Solve(const net::TcpConnectionPtr& conn, const std::string& puzzle, const std::string& id);

private:
	net::EventLoop* loop_;
	net::TcpServer server_;
	// ThreadPool thread_pool_;
	const static int kLength;
};

#endif // _SUDO_SERVER_H_