#include "netlib/net/event/event_loop.h"
#include "sudo_server.h"

int main() {
	LOG_INFO << "pid = " << getpid();
	net::EventLoop loop;
	net::InetAddress listen_addr(6666);
	SudoServer server(&loop, listen_addr, 2);
	server.Start();
	loop.Loop();
	return 0;
}