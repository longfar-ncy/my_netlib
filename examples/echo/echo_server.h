#pragma once

#include "netlib/net/tcp_server.h"

class EchoServer {
public:
	EchoServer(netlib::net::EventLoop* loop, const netlib::net::InetAddress& addr);
	void start();

private:
	void onConnection(const netlib::net::TcpConnectionPtr& conn);
	void onMessage(const netlib::net::TcpConnectionPtr&, netlib::net::Buffer*, netlib::Timestamp);

private:
	netlib::net::EventLoop* loop_;
	netlib::net::TcpServer server_;
};
