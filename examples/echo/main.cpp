#include "echo_server.h"
#include "netlib/base/logger.h"
#include "netlib/net/event/event_loop.h"
#include "netlib/net/socket/inet_address.h"

using namespace netlib;

int main() {
	LOG_INFO << "pid = " << getpid();
	net::EventLoop loop;
	net::InetAddress listen_addr(6666);
	EchoServer server(&loop, listen_addr);
	server.start();
	loop.Loop();
	return 0;
}
