# my_netlib

## 介绍
为学习多线程服务器编程，参照muduo自己实现的轻量级网络库

## 项目结构
```
.
├── build_support       # 用于代码规范的工具
├── examples            # 示例代码
│   ├── atomic_incr     # 基于 protobuf 的原子递增服务器/客户端
│   ├── chat            # 基于 TCP 的多人聊天服务器/客户端
│   ├── echo            # 基于 TCP 的回声服务器/客户端
│   ├── kvstore         # 基于 leveldb + protobuf 的 KV 存储服务器/客户端
│   ├── simple          # 简单示例
│   └── sudoku          # 基于 TCP 的数独求解服务器/客户端
├── include             # 头文件目录
│   └── netlib
│       ├── base        # 工具（日志、线程池等）
│       ├── net         # 网络相关
│       └── rpc         # RPC 相关
├── README.md           # 文档
├── src                 # 源码目录
│   ├── base
│   ├── net
│   └── rpc
└── tests               # 测试文件目录
    ├── base
    ├── net
    └── rpc
```

## 使用示例
以一个 Echo 服务器为例，详见 `./examples/simple` 目录：

### 服务端
```C++
int main() {
	using namespace netlib;
	net::InetAddress addr("127.0.0.1", 28709); // 监听端口
	net::EventLoop loop;                       // 事件监听主循环
	net::TcpServer server(&loop, addr);        // 构造 server
	server.SetMessageCallback(                 // 为 server 设置处理消息的回调函数
	    [](const net::TcpConnectionPtr& conn, net::Buffer* buf, Timestamp ts) {
		    std::string msg(buf->RetrieveAllAsString());
		    LOG_INFO << conn->Name() << " echo " << msg.size() << "bytes, "
		             << "data received at " << ts.ToString();
		    conn->Send(msg);
	    });
	server.Start(); // 服务器开始监听
	loop.Loop();    // 主事件循环开始
}
```

### 客户端
```C++
int main() {
	using namespace netlib;
	net::InetAddress addr("127.0.0.1", 28709);            // 目标地址
	net::EventLoopThread loop_thread;                     // 事件循环线程
	net::TcpClient client(loop_thread.StartLoop(), addr); // 构造客户端
	client.SetMessageCallback(                            // 设置处理收到消息的回调函数
	    [](const net::TcpConnectionPtr& conn, net::Buffer* buf, Timestamp ts) {
		    std::string msg(buf->RetrieveAllAsString());
		    std::cout << "Receive: " << msg << std::endl; 
	    });
	client.Connect();            // 客户端连接服务端，成功or失败后返回
	if (!client.IsConnected()) { // 若连接不成功，则退出程序
		LOG_FATAL << "Failed to connect to server";
	}

	// 读取输入的每一行字符串并通过客户端发送给服务端，直到 exit 或 quit
	std::string line;
	while (std::getline(std::cin, line)) {
		if (line == "exit" || line == "quit") {
			break;
		}
		client.Send(line);
	}

	client.Disconnect(); // 断开连接
}
```
