#include "netlib/net/event/event_loop_threadpool.h"

#include <iostream>
#include <memory>
#include <vector>

#include "netlib/net/event/event_loop.h"
#include "gtest/gtest.h"

using namespace netlib::net;

TEST(EventLoopPoolTest, SimpleTest) {
	EventLoop base_loop(true);
	EventLoopThreadPool(&base_loop, true, 2);
}
