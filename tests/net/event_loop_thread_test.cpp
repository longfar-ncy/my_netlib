#include "netlib/net/event/event_loop_thread.h"

#include <iostream>
#include <memory>
#include <vector>

#include "netlib/net/event/event_loop.h"
#include "gtest/gtest.h"

using namespace netlib::net;

TEST(EventLoopThreadTest, SimpleTest) {
	EventLoopThread loop_thread(true);
	EventLoop* loop = loop_thread.StartLoop();

	loop->RunAfter(2, [loop] {
		std::cout << "Time is out" << std::endl;
		loop->Quit();
	});
}

TEST(EventLoopThreadTest, MutiThreadLoop) {
	std::vector<std::unique_ptr<EventLoopThread>> ths(4);
	for (auto& th : ths) {
		th = std::make_unique<EventLoopThread>(true);
		EventLoop* loop = th->StartLoop();
		loop->RunAfter(1, [loop] {
			std::cout << "Time is out" << std::endl;
			loop->Quit();
		});
	}
	sleep(2);
}