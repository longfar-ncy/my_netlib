#include "netlib/base/repl.h"

#include <gtest/gtest.h>

#include <iostream>

using netlib::REPL;

TEST(REPLTest, SimpleTest) {
	REPL repl;
	repl.AddCommand("test", [](char* args) {
		std::cout << (args == nullptr ? "nullptr" : const_cast<const char*>(args))
		          << std::endl; // NOLINT
		return 0;
	});
	repl.Run();
}
