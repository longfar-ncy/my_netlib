cmake_minimum_required(VERSION 3.22)

include(GoogleTest)

file(GLOB_RECURSE NETLIB_TEST_SOURCE "${PROJECT_SOURCE_DIR}/tests/*/*test.cpp")

foreach(netlib_test_source ${NETLIB_TEST_SOURCE})
  get_filename_component(netlib_test_filename ${netlib_test_source} NAME)
  string(REPLACE ".cpp" "" netlib_test_name ${netlib_test_filename})

  add_executable(${netlib_test_name} EXCLUDE_FROM_ALL ${netlib_test_source})

  gtest_discover_tests(${netlib_test_name}                                                                                                              33             EXTRA_ARGS
    --gtest_color=auto
    --gtest_output=xml:${CMAKE_BINARY_DIR}/test/${bustub_test_name}.xml
    --gtest_catch_exceptions=0
    DISCOVERY_TIMEOUT 120
    PROPERTIES
    TIMEOUT 120
  )

  target_include_directories(${netlib_test_name}
    PRIVATE ${PROJECT_SOURCE_DIR}
  )

  target_link_directories(${netlib_test_name}
    PUBLIC ${STAGED_INSTALL_PREFIX}/lib
  )

  target_link_libraries(${netlib_test_name} 
    base 
    net
    rpc
    libgtest.a
    libgtest_main.a
    ${PROTOBUF_LIB}
  )

  set_target_properties(${netlib_test_name} PROPERTIES
    RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/tests
  )
endforeach() 
